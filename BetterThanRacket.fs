namespace BetterThanRacket

open System

module BinaryTreeHausübung =
    type BinaryTree = {
        value: int
        left: BinaryTree option
        right: BinaryTree option
    }

    let smallTree = {
        value = 5;
        left = Some {
            value = 3;
            left = Some {
                value = 1; left = None; right = None
            }
            right = Some {
                value = 4; left = None; right = None
            }
        }
        right = Some {
            value = 9;
            left = Some {
                value = 6; left = None; right = None
            }
            right = Some {
                value = 11; left = None; right = None
            }
        }
    }

    let rec BinaryTreeContains (value:int) (btn:BinaryTree) = 
        if btn.value = value then true
        else 
            if value < btn.value then
                match btn.left with
                | Some BinaryTree -> BinaryTreeContains value btn.left.Value
                | None -> false
            else
                if Option.isSome btn.right then
                    BinaryTreeContains value btn.right.Value
                else false

    let rec InsertIntoBinaryTree (value:int) (btn:BinaryTree) =
        0