// Learn more about F# at http://fsharp.org

open System

open SomeNamespace.SomeModule

open BetterThanRacket

open System.Numerics
open System.Collections.Generic

//open System.Windows.Forms

(* let rec fib n =
    if n = 0 then 0
    else if n = 1 then 1
    else fib(n-2) + fib(n-1) *)


(*
let rec fib n =
    match n with
    | 0 -> 0
    | 1 -> 1
    | _ -> fib(n-2) + fib(n-1)
*)


(* let rec fib n =
    match n with
    | 0 | 1 -> n
    | _ -> fib(n-2) + fib (n-1) *)


(*
let rec factorial n =
    if n = 0 then 1
    else n * factorial(n-1)
*)

(*
let rec factorial n =
    match n with
    | 0 -> 1
    | _ -> n * factorial(n-1)
*)

type Student = {
    LastName : string
    FirstName : string
    EnrollmentNumber : int
    Fee:int
}

(*
let sumOfStudentFees stud = 
    stud
    // You don't even need to filter all students because they are
    // automatically students
    |> List.map (fun x -> x.Fee)
    |> List.fold (fun x y -> x + y) 0
*)

(*
let sumOfStudentFees stud = 
    stud
    // You don't even need to filter all students because they are
    // automatically students
    |> List.map (fun x -> x.Fee)
    |> List.fold (( + )) 0
*)

(*let sumOfStudentFees stud = 
    List.sumBy (fun x -> x.Fee) stud
*)
[<EntryPoint>]
let main argv =
    (*let result = fib 10
    printfn "%d" result

    let myList = [1;4;5;3;2]
    List.map (fun x -> printfn "%d" x) myList |> ignore*)

    (*let marcus = { LastName = "Müller"; FirstName = "Marcus"; EnrollmentNumber = 0; Fee = 100 }
    let marc = { LastName = "Müller"; FirstName = "Marc"; EnrollmentNumber = 0; Fee = 100 }

    let listOfStudents = [ marc; marcus ]

    printfn "%d" (sumOfStudentFees(listOfStudents))*)

    let num = new BigInteger(122231)

    let myList = [1;3;4;6;7]

    myList
    |> List.map ((+) 5)
    |> printfn "%A"

    num.ToString()
    |> printfn "%s"

    printfn "%d" theAnswer

    printfn "%b" (BetterThanRacket.BinaryTreeHausübung.BinaryTreeContains 56 BinaryTreeHausübung.smallTree)

    printfn "%A" (Euklid.euklid 128 36)
    printfn "%A" (Euklid.erwEuklid 128 36)

    0 // return an integer exit code
